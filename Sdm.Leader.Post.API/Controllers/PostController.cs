﻿using Microsoft.AspNetCore.Mvc;
using Sdm.Leader.Core.DTO;
using Sdm.Leader.Core.Service;

namespace Sdm.Leader.Post.API.Controllers
{
    [Route("{controller}")]
    public sealed class PostController : Controller
    {
        private readonly IPostService _postService;

        public PostController(IPostService postService)
        {
            _postService = postService ?? throw new ArgumentNullException(nameof(postService));
        }

        [HttpPost("{action}")]
        public IActionResult Create([FromBody] PostDTO post, CancellationToken cancellationToken)
        {
            _postService.Create(post, cancellationToken);

            return Ok();
        }
    }
}
