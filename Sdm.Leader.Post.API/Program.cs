using Microsoft.EntityFrameworkCore;
using Npgsql;
using Sdm.Leader.Core.Service;
using Sdm.Leader.Post.Data;
using Sdm.Leader.Post.Service;
using NotificationServiceHttp = Sdm.Leader.Core.Service.Http.NotificationService;

namespace Sdm.Leader.Post.API
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var builder = WebApplication.CreateBuilder(args);

            builder.Services.AddDbContextFactory<MhgDevContext>(options =>
            {
                var dataSourceBuilder = new NpgsqlDataSourceBuilder
                (
                    builder.Configuration.GetValue<string>("ConnectionStrings:MhgDev")
                );

                var dataSource = dataSourceBuilder.Build();

                options.UseNpgsql(dataSource);
            });

            builder.Services.AddSingleton<NotificationServiceHttp>(_ => new NotificationServiceHttp(
                builder.Configuration.GetValue<string>("NotificationService:Url")!
            ));
            builder.Services.AddSingleton<INotificationService>(options => options.GetRequiredService<NotificationServiceHttp>());

            builder.Services.AddSingleton<PostService>();
            builder.Services.AddSingleton<IPostService>(options => options.GetRequiredService<PostService>());

            builder.Services.AddControllers();

            var app = builder.Build();

            app.MapControllers();

            app.Run();
        }
    }
}