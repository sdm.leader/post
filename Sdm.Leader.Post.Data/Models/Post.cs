﻿using Sdm.Leader.Core.DTO;

namespace Sdm.Leader.Post.Data.Models
{
    public class Post
    {
        public long Id { get; set; }

        public string? Data { get; set; }

        public PostDTO ToPostDTO()
        {
            return new() 
            { 
                Id = Id,
                Data = Data
            };
        }

        public static Post FromPostDTO(PostDTO data)
        {
            return new() 
            { 
                Id = data.Id,
                Data = data.Data
            };
        }
    }
}
