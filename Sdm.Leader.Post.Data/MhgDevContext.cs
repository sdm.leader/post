﻿using Microsoft.EntityFrameworkCore;
using PostEntity = Sdm.Leader.Post.Data.Models.Post;

namespace Sdm.Leader.Post.Data
{
    public sealed class MhgDevContext : DbContext
    {
        public MhgDevContext(DbContextOptions<MhgDevContext> options) : base(options) { }

        public DbSet<PostEntity> Posts { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<PostEntity>(entity =>
            {
                entity.HasKey(e => e.Id).HasName("posts_pkey");

                entity.ToTable("posts");

                entity.Property(e => e.Id).HasColumnName("id");
                entity.Property(e => e.Data).HasColumnName("data");
            });
        }
    }
}
