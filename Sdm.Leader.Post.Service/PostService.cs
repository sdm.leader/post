﻿using Microsoft.EntityFrameworkCore;
using Sdm.Leader.Core.DTO;
using Sdm.Leader.Core.Service;
using Sdm.Leader.Post.Data;
using PostEntity = Sdm.Leader.Post.Data.Models.Post;

namespace Sdm.Leader.Post.Service
{
    public sealed class PostService : IPostService
    {
        private readonly IDbContextFactory<MhgDevContext> _contextFactory;
        private readonly INotificationService _notificationService;

        public PostService(IDbContextFactory<MhgDevContext> contextFactory, INotificationService notificationService)
        {
            _contextFactory = contextFactory ?? throw new ArgumentNullException(nameof(contextFactory));
            _notificationService = notificationService ?? throw new ArgumentNullException( nameof(notificationService));
        }

        public void Create(PostDTO post, CancellationToken cancellationToken)
        {
            using var context = _contextFactory.CreateDbContext();

            context.Posts.Add
            (
                PostEntity.FromPostDTO(post)
            );

            _notificationService.Create
            (
                new() 
                { 
                    Data = post.Data
                },
                cancellationToken
            );

            context.SaveChanges();
        }
    }
}